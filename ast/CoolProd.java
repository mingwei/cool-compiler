package ast;
import beaver.Symbol;

public class CoolProd extends CoolNode{
	public Symbol sa;

	public CoolProd(CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 28;
	}

	public CoolProd(CoolNode n1, Symbol o1, CoolNode n2,int n)
	{
		super(n,n1,n2);
		this.nodeType = 28;
		sa = o1;
	}

	public void accept(){
		this.nodeType = 28;
		printer.printStart("Prod");
		if(this.num==3){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	}

}

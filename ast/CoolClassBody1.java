package ast;

public class CoolClassBody1 extends CoolNode{



	public CoolClassBody1(CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 14;
		this.nodeType = 14;
	}

	public CoolClassBody1(CoolNode n1, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 14;
		this.nodeType = 14;
	}

	public void accept(){
		this.nodeType = 14;
		printer.printStart("ClassBody1");
		if(this.num==1){
			this.a.accept();
		}
		if(this.num==2){
			this.a.accept();
			printer.printComma();
			this.b.accept();
		}	
		printer.printEnd();
	}

}

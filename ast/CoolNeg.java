package ast;
import beaver.Symbol;

public class CoolNeg extends CoolNode{

	public Symbol sa;

	public CoolNeg(CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 25;
	}

	public CoolNeg(Symbol o1, CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 25;
		sa = o1;
	}


	public void accept(){
		this.nodeType = 25;
		printer.printStart("Neg");
		if(this.num==3){
			this.a.accept();
		} else {
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
		}
		printer.printEnd();
	}

}

package ast;
import beaver.Symbol;

public class CoolExpr extends CoolNode{
	public Symbol sa,sb;

	public CoolExpr(Symbol o1, Symbol o2, CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 18;
		sa=o1;
		sb=o2;
	}

	public CoolExpr(CoolNode n1)
	{
		super(2,n1);
		this.nodeType = 18;
	}

	public void accept(){
		this.nodeType = 18;
		printer.printStart("Expr");
		if(num==1){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			this.a.accept();
		} else {
			this.a.accept();
		}
		printer.printEnd();
	}

}

package ast;

public class CoolBlock extends CoolNode{

	public CoolBlock(CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 3;
	}

	public void accept(){
		this.nodeType = 3;
		printer.printStart("Block");
		this.a.accept();
		printer.printEnd();
	}

}

package ast;

import beaver.Symbol;

public abstract class CoolNode extends Symbol{
	public int num;
	public CoolNode a,b,c;
	public Myjson printer;
	public int nodeType=0;
	public String returnType = "error";

	public String variableName = "null";
	public int reg=-1;

	public String value = "null";
	
	public CoolNode(int id){
		printer = new Myjson();
		this.num=id;
	}
	public CoolNode(int id,CoolNode n1){
		printer = new Myjson();
		this.num=id;
		this.a=n1;
	}
	public CoolNode(int id,CoolNode n1, CoolNode n2){
		printer = new Myjson();
		this.num=id;
		this.a=n1;
		this.b=n2;
	}
	public CoolNode(int id,CoolNode n1, CoolNode n2, CoolNode n3){
		printer = new Myjson();
		this.num=id;
		this.a=n1;
		this.b=n2;
		this.c=n3;
	}
	public abstract void accept();
}

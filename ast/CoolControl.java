package ast;
import beaver.Symbol;

public class CoolControl extends CoolNode{

	public Symbol sa,sb,sc,sd;

	public CoolControl(Symbol o1, Symbol o2, CoolNode n1, Symbol o3, CoolNode n2, Symbol o4, CoolNode n3){
		super(1,n1,n2,n3);
		this.nodeType = 15;
		sa=o1;
		sb=o2;
		sc=o3;
		sd=o4;
	}
	public CoolControl(Symbol o1, Symbol o2, CoolNode n1, Symbol o3, CoolNode n2){
		super(2,n1,n2);
		this.nodeType = 15;
		sa=o1;
		sb=o2;
		sc=o3;
	}
	public CoolControl(CoolNode n1){
		super(3,n1);
		this.nodeType = 15;
	}

	public void accept(){
		this.nodeType = 15;
		printer.printStart("Control");
		if(this.num==1){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sc);
			printer.printComma();
			this.b.accept();
			printer.printComma();
			printer.printTerminal(sd);
			printer.printComma();
			this.c.accept();
		} else if(this.num==2) {
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sc);
			printer.printComma();
			this.b.accept();
		} else {
			this.a.accept();
		}
		printer.printEnd();
	}

}

package ast;
import beaver.Symbol;

public class CoolSum extends CoolNode{
	public Symbol sa;

	public CoolSum(CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 31;
	}

	public CoolSum(CoolNode n1, Symbol o1, CoolNode n2,int n)
	{
		super(n,n1,n2);
		this.nodeType = 31;
		sa = o1;
	}

	public void accept(){
		this.nodeType = 31;
		printer.printStart("Sum");
		if(this.num==3){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	}

}

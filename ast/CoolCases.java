package ast;
import beaver.Symbol;

public class CoolCases extends CoolNode{

	public Symbol sa,sb;

	public CoolCases(Symbol o1, CoolNode n1, Symbol o2)
	{
		super(1,n1);
		this.nodeType = 6;
		sa = o1;
		sb = o2;
	}

	public void accept(){
		this.nodeType = 6;
		printer.printStart("Cases");
		printer.printTerminal(sa);
			printer.printComma();
		this.a.accept();
			printer.printComma();
		printer.printTerminal(sb);
		printer.printEnd();
	}

}

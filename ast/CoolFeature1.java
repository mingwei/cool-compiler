package ast;
import beaver.Symbol;

public class CoolFeature1 extends CoolNode{
	public Symbol sa;

	public CoolFeature1(Symbol o1)
	{
		super(1);
		this.nodeType = 20;
		sa = o1;
	}

	public CoolFeature1(CoolNode n1)
	{
		super(2,n1);
		this.nodeType = 20;
	}

	public void accept(){
		this.nodeType = 20;
		printer.printStart("Feature1");
		if(this.num==1){
			printer.printTerminal(sa);
		} else {
			this.a.accept();
		}
		printer.printEnd();
	}

}

package ast;
import beaver.Symbol;

public class CoolProgram extends CoolNode{
	public CoolProgram(CoolNode expr)
	{
		super(1,expr);
		this.nodeType = 29;
	}
	public CoolProgram(CoolNode n1, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 29;
	}

	public void accept(){
		this.nodeType = 29;
		printer.printStart("Program");
		if(this.num==1){
			this.a.accept();
		} else if(this.num==2) {
			this.a.accept();
			printer.printComma();
			this.b.accept();
			//printer.printComma();
		}
		printer.printEnd();
	}
}

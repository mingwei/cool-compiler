package ast;
import beaver.Symbol;

public class CoolCases3 extends CoolNode{

	public Symbol sa,sb,sc;

	public CoolCases3(Symbol o1, Symbol o2, Symbol o3)
	{
		super(1);
		sa = o1;
		sb = o2;
		sc = o3;
		this.nodeType = 9;
	}
	public CoolCases3(Symbol o1)
	{
		super(2);
		sa = o1;
		this.nodeType = 9;
	}

	public void accept(){
		this.nodeType = 9;
		printer.printStart("Cases3");
		if(this.num==1){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			printer.printTerminal(sc);
		} else {
			printer.printTerminal(sa);
		}
		printer.printEnd();
	}

}

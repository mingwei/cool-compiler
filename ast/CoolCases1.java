package ast;

public class CoolCases1 extends CoolNode{


	public CoolCases1(CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 7;
	}

	public CoolCases1(CoolNode n1, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 7;
	}

	public void accept(){
		this.nodeType = 7;
		printer.printStart("Cases1");
		if(this.num==1){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	
	}

}

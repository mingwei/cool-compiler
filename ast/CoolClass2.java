package ast;
import beaver.Symbol;

public class CoolClass2 extends CoolNode{
	public Symbol sa;
	public CoolClass2(Symbol o1,CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 12;
		sa = o1;
	}

	public CoolClass2(Symbol o1)
	{
		super(2);
		this.nodeType = 12;
		sa = o1;
	}

	public void accept()
	{
		this.nodeType = 12;
		printer.printStart("Class2");
		if(this.num==1)
		{	
		 	printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
		}
		if(this.num==2)
		{
			printer.printTerminal(sa);
		}
		printer.printEnd();
	}
}

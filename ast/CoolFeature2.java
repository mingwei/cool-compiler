package ast;
import beaver.Symbol;

public class CoolFeature2 extends CoolNode{
	public Symbol sa,sb,sc;

	public CoolFeature2(Symbol o1,Symbol o2, Symbol o3,CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 21;
		sa = o1;
		sb = o2;
		sc = o3;
	}

	public CoolFeature2(Symbol o1,Symbol o2)
	{
		super(2);
		this.nodeType = 21;
		sa = o1;
		sb = o2;
	}

	public void accept(){
		this.nodeType = 21;
		printer.printStart("Feature2");
		if(this.num==1){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			printer.printTerminal(sc);
			printer.printComma();
			this.a.accept();
		} else {
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
		}
		printer.printEnd();
	}

}

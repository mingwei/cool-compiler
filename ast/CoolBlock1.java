package ast;
import beaver.Symbol;

public class CoolBlock1 extends CoolNode{
	public Symbol sa;

	public CoolBlock1(CoolNode n1, Symbol o1, CoolNode n2,int n)
	{
		super(n,n1,n2);
		sa = o1;
		this.nodeType = 4;
	}

	public CoolBlock1(CoolNode n1,CoolNode n2, Symbol o1, CoolNode n3,int n)
	{
		super(n,n1,n2,n3);
		this.nodeType = 4;
		sa = o1;
	}

	public void accept(){
		this.nodeType = 4;
		printer.printStart("Block1");
		if(this.num==1 || this.num==2)
		{
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		else
		{
			this.a.accept();
			printer.printComma();
			this.b.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.c.accept();
		}
		printer.printEnd();

	}

}

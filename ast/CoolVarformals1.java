package ast;
import beaver.Symbol;

public class CoolVarformals1 extends CoolNode{
	public Symbol sa,sb,sc,sd,se;
	public CoolVarformals1(Symbol o1,Symbol o2,Symbol o3,Symbol o4)
	{
		super(1);
		this.nodeType = 33;
		this.sa = o1;
		this.sb = o2;
		this.sc = o3;
		this.sd = o4;
	}

	public CoolVarformals1(Symbol o1,Symbol o2,Symbol o3,Symbol o4,Symbol o5,CoolNode n1)
	{
		super(2,n1);
		this.nodeType = 33;
		this.sa = o1;
		this.sb = o2;
		this.sc = o3;
 		this.sd = o4;
		this.se = o5;
	}

	public void accept(){
		this.nodeType = 33;
		printer.printStart("Varformals1");
		if(this.num==1)
		{
			printer.printTerminal(this.sa);
			printer.printComma();
			printer.printTerminal(this.sb);
			printer.printComma();
			printer.printTerminal(this.sc);
			printer.printComma();
			printer.printTerminal(this.sd);
		}
		if(this.num==2)
		{
			printer.printTerminal(this.sa);
			printer.printComma();
			printer.printTerminal(this.sb);
			printer.printComma();
			printer.printTerminal(this.sc);
			printer.printComma();
			printer.printTerminal(this.sd);
			printer.printComma();
			printer.printTerminal(this.se);
			printer.printComma();
			this.a.accept();
		}
		printer.printEnd();	

	}

}

package ast;
import beaver.Symbol;

public class CoolFormals1 extends CoolNode{
	public Symbol sa,sb,sc,sd;

	public CoolFormals1(Symbol o1,Symbol o2,Symbol o3)
	{
		super(1);
		this.nodeType = 23;
		sa = o1;
		sb = o2;
		sc = o3;
	}

	public CoolFormals1(Symbol o1,Symbol o2,Symbol o3,Symbol o4,CoolNode n1)
	{
		super(2,n1);
		this.nodeType = 23;
		sa = o1;
		sb = o2;
		sc = o3;
		sd = o4;
	}

	public void accept(){
		this.nodeType = 23;
		printer.printStart("Formals1");
		if(this.num==1)
		{
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			printer.printTerminal(sc);
		}
		if(this.num==2)
		{
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			printer.printTerminal(sc);
			printer.printComma();
			printer.printTerminal(sd);
			printer.printComma();
			this.a.accept();
		}
		printer.printEnd();	

	}

}

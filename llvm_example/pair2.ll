;
;
;  Classes, objects, and methods in llvm
;
; In a Java-ish pseudocode: 
;
; class Single {
;     int x;
;     void incx(int delta) { this.x += delta; }
;     int getval() { return this.x; }
;

target triple = "x86_64-apple-macosx10.7.0"   ;; Ugh, apparently have to tell it


%Any = type i32  ; 'Any' class not defined yet


; Declare the layout of the class structure

%class_Single = type { 
    %Any*,                        ; Superclass pointer
    %obj_Single* ( )*,            ; constructor
    void ( %obj_Single*, i32)*,   ; void incx( this, int )
    i32 (%obj_Single*)*           ; int getval( this )
    }

; Declare layout of the object structure

%obj_Single = type { 
	%class_Single*, 				; class
	i32 							; x
	}

; The actual class object --- there is just one of these, however many 
; objects of this type there may be

@Single = global %class_Single {
    %Any* null, 						; Just a placeholder
    %obj_Single* ( )* @Single_constructor, 
    void (%obj_Single*, i32)* @Single_incx, 
    i32 (%obj_Single*)* @Single_getval
    }

; Methods in Single

define %obj_Single* @Single_constructor( ) {
   %memchunk = call i8* @malloc(i64 8)              ; 2 4-byte words of memory for object
   %as_obj = bitcast i8* %memchunk to %obj_Single*  ; cast it to a Single object
   ;; Allocation (above) should really be separate from initialization (below).  Allocation is 
   ;; done once, while initialization recursively calls superclass initializers
   %field_class_ptr = getelementptr inbounds %obj_Single* %as_obj, i32 0, i32 0   
   store %class_Single* @Single, %class_Single** %field_class_ptr
   %field_x = getelementptr inbounds %obj_Single* %as_obj, i32 0, i32 1
   store i32 0, i32* %field_x
   ret %obj_Single* %as_obj
   }
   

define void @Single_incx( %obj_Single* %this, i32 %delta) {
	%xaddr = getelementptr  %obj_Single* %this, i32 0, i32 1
	%xval = load i32* %xaddr
	%sum = add i32 %xval, %delta
	store i32 %sum, i32* %xaddr
	ret void
}

define i32 @Single_getval( %obj_Single* %this) {
	%xaddr = getelementptr  %obj_Single* %this, i32 0, i32 1
	%xval = load i32* %xaddr
	ret i32 %xval
}
	
;  Woohoo.  Now let's create an object, call a couple methods, and return a value
define i32 @ll_main(i32 %delta) {
	%newish = getelementptr inbounds %class_Single* @Single, i32 0, i32 1
	%construct = load %obj_Single*( )** %newish
	%my_obj = call %obj_Single* %construct( )
	;; Call method incx with argument %delta
	%myobj_class = getelementptr inbounds %obj_Single* %my_obj, i32 0, i32 0
	%classptr = load %class_Single** %myobj_class
	%incx_method = getelementptr inbounds %class_Single* %classptr, i32 0, i32 2
	%call_me_maybe = load void (%obj_Single*, i32)** %incx_method
	call void %call_me_maybe(%obj_Single* %my_obj, i32 %delta)
	;; Do it again, just to see the effect
	call void %call_me_maybe(%obj_Single* %my_obj, i32 %delta)
	;; Now get the value with getval method
	%getval_method_addr = getelementptr inbounds %class_Single* %classptr, i32 0, i32 3
	%getval_method = load i32 (%obj_Single*)** %getval_method_addr
	%result = call i32 %getval_method(%obj_Single* %my_obj)
	ret i32 %result
}
    
declare i8* @malloc(i64)



	



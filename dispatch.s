; ModuleID = 'dispatch.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.8.0"

%struct.single_obj = type { %struct.single_class*, i32 }
%struct.single_class = type { i32, {}*, i32 (%struct.single_obj*)* }

define void @method_incx(%struct.single_obj* %this) nounwind uwtable ssp {
  %1 = alloca %struct.single_obj*, align 8
  store %struct.single_obj* %this, %struct.single_obj** %1, align 8
  %2 = load %struct.single_obj** %1, align 8
  %3 = getelementptr inbounds %struct.single_obj* %2, i32 0, i32 1
  %4 = load i32* %3, align 4
  %5 = add nsw i32 %4, 1
  %6 = load %struct.single_obj** %1, align 8
  %7 = getelementptr inbounds %struct.single_obj* %6, i32 0, i32 1
  store i32 %5, i32* %7, align 4
  ret void
}

define i32 @method_getx(%struct.single_obj* %this) nounwind uwtable ssp {
  %1 = alloca %struct.single_obj*, align 8
  store %struct.single_obj* %this, %struct.single_obj** %1, align 8
  %2 = load %struct.single_obj** %1, align 8
  %3 = getelementptr inbounds %struct.single_obj* %2, i32 0, i32 1
  %4 = load i32* %3, align 4
  ret i32 %4
}

define i32 @ll_main(i32 %junk) nounwind uwtable ssp {
  %1 = alloca i32, align 4
  %the_class = alloca %struct.single_class, align 8
  %the_obj = alloca %struct.single_obj*, align 8
  %vtable = alloca %struct.single_class*, align 8
  %method = alloca void (%struct.single_obj*)*, align 8
  %meth2 = alloca i32 (%struct.single_obj*)*, align 8
  %result = alloca i32, align 4
  store i32 %junk, i32* %1, align 4
  %2 = getelementptr inbounds %struct.single_class* %the_class, i32 0, i32 1
  %3 = bitcast {}** %2 to void (%struct.single_obj*)**
  store void (%struct.single_obj*)* @method_incx, void (%struct.single_obj*)** %3, align 8
  %4 = getelementptr inbounds %struct.single_class* %the_class, i32 0, i32 2
  store i32 (%struct.single_obj*)* @method_getx, i32 (%struct.single_obj*)** %4, align 8
  %5 = call i8* @malloc(i64 16)
  %6 = bitcast i8* %5 to %struct.single_obj*
  store %struct.single_obj* %6, %struct.single_obj** %the_obj, align 8
  %7 = load %struct.single_obj** %the_obj, align 8
  %8 = getelementptr inbounds %struct.single_obj* %7, i32 0, i32 0
  store %struct.single_class* %the_class, %struct.single_class** %8, align 8
  %9 = load %struct.single_obj** %the_obj, align 8
  %10 = getelementptr inbounds %struct.single_obj* %9, i32 0, i32 1
  store i32 0, i32* %10, align 4
  %11 = load %struct.single_obj** %the_obj, align 8
  %12 = getelementptr inbounds %struct.single_obj* %11, i32 0, i32 0
  %13 = load %struct.single_class** %12, align 8
  store %struct.single_class* %13, %struct.single_class** %vtable, align 8
  %14 = load %struct.single_class** %vtable, align 8
  %15 = getelementptr inbounds %struct.single_class* %14, i32 0, i32 1
  %16 = bitcast {}** %15 to void (%struct.single_obj*)**
  %17 = load void (%struct.single_obj*)** %16, align 8
  store void (%struct.single_obj*)* %17, void (%struct.single_obj*)** %method, align 8
  %18 = load void (%struct.single_obj*)** %method, align 8
  %19 = load %struct.single_obj** %the_obj, align 8
  call void %18(%struct.single_obj* %19)
  %20 = load %struct.single_class** %vtable, align 8
  %21 = getelementptr inbounds %struct.single_class* %20, i32 0, i32 2
  %22 = load i32 (%struct.single_obj*)** %21, align 8
  store i32 (%struct.single_obj*)* %22, i32 (%struct.single_obj*)** %meth2, align 8
  %23 = load i32 (%struct.single_obj*)** %meth2, align 8
  %24 = load %struct.single_obj** %the_obj, align 8
  %25 = call i32 %23(%struct.single_obj* %24)
  store i32 %25, i32* %result, align 4
  %26 = load i32* %result, align 4
  ret i32 %26
}

declare i8* @malloc(i64)
